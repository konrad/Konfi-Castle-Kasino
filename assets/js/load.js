const app = new Vue({
    el: '#table',
    template: `
<div style="width: 99%; margin: 0 auto;padding-top: 10px;">
    <div class="ui error message" style="display: block;" v-if="error !== ''">{{ error }}</div>
    <table class="ui celled striped inverted table" v-if="error === ''">
        <thead>
        <tr class="top">
            <th scope="col">Platz</th>
            <th scope="col" v-if="mode === 0">Name</th>
            <th scope="col" v-if="mode === 1">Gemeinde</th>
            <th scope="col" v-if="mode === 0">Gemeinde</th>
            <th scope="col" v-if="mode === 1">KonfiCoins pro Person</th>
            <th scope="col">Eingezahlte KonfiCoins Gesamt</th>
        </tr>
        </thead>
        <tbody>
        <tr v-if="data.length === 0">
            <td colspan="4">Laden...</td>
        </tr>
        <tr v-for="(item, i) in data">
            <td>{{ (i + 1) }}.</td>
            <td>{{ item.name }}</td>
            <td v-if="mode === 0">{{ item.gemeinde }}</td>
            <td v-if="mode === 1">{{ parseFloat((item.coins_quota).toFixed(2)).toLocaleString('de-DE') }}</td>
            <td>{{ item.kcoins.toLocaleString('de-DE') }}</td>
        </tr>
        </tbody>
    </table>
</div>`,
    data() {
        return {
            mode: 1,
            data: [],
            error: '',
        }
    },
    beforeMount() {
        this.mode = mode
    },
    mounted() {
        if (typeof (EventSource) === "undefined") {
            this.error = 'Diese Browser wird nicht unterstützt.'
            return
        }

        let source = new EventSource('/events');
        source.onmessage = e => {
            console.debug('unsupported event!', e)
        };
        source.addEventListener('init', e => {
            // We assume we get the data sorted, so we won't sort it here
            this.data = JSON.parse(e.data)
        })
        source.addEventListener('update', e => {
            this.update(JSON.parse(e.data))
        })
        source.addEventListener('create', e => {
            this.create(JSON.parse(e.data))
        })
        source.addEventListener('delete', e => {
            this.delete(JSON.parse(e.data))
        })
    },
    methods: {
        sortData() {
            this.data.sort((a, b) => {
                if (this.mode === 1) { // Gemeinden
                    return a.coins_quota > b.coins_quota ? -1 : 1
                }
                // Sonst kofis
                return a.kcoins > b.kcoins ? -1 : 1
            })
        },
        update(updatedData) {
            for (const i in this.data) {
                if (this.data[i].id === updatedData.id) {
                    this.data[i] = updatedData
                }
            }
            this.sortData()
        },
        create(createdData) {
            this.data.push(createdData)
            this.sortData()
        },
        delete(deletedData) {
            for (const i in this.data) {
                if (this.data[i].id === deletedData.id) {
                    this.data.splice(i, 1)
                }
            }
            this.sortData()
        },
    },
});
