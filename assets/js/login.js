$(document).ready(function () {
    $('#loginform').submit(function (ev) {
        ev.preventDefault();
        login();
    });
});

function login() {
    let pass = $('#password').val();
    if(pass === ""){
        $('#msg').html('<div class="ui error message" style="display: block;">Bitte gib ein Passwort ein.</div>');
    } else {
        $.ajax({
            url: '/login',
            method: 'POST',
            data: 'password=' + pass,
            success: function (msg, ev) {
                $('#msg').html('<div class="ui success message" style="display: block;">Erfolgreich eingeloggt.</div>');
                location.reload();
            },
            error: function (msg, ev) {
                $('#msg').html('<div class="ui error message" style="display: block;">Falsches Passwort.</div>');
            }
        })
    }
}