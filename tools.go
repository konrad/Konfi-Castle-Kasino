// +build tools

package tools

// This file is needed for go mod to recognize the tools we use.

import (
	_ "golang.org/x/lint/golint"
	_ "src.techknowlogick.com/xgo"
)
