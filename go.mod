module git.kolaente.de/konrad/Konfi-Castle-Kasino

go 1.12

require (
	github.com/asticode/go-astilectron v0.8.0
	github.com/asticode/go-astitools v1.1.0 // indirect
	github.com/go-ini/ini v1.46.0
	github.com/go-xorm/xorm v0.7.6
	github.com/gorilla/sessions v1.2.0
	github.com/labstack/echo-contrib v0.6.0
	github.com/labstack/echo/v4 v4.1.6
	github.com/labstack/gommon v0.3.0
	github.com/mattn/go-sqlite3 v1.11.0
	github.com/pkg/browser v0.0.0-20180916011732-0a3d74bf9ce4
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337 // indirect
	golang.org/x/crypto v0.0.0-20190829043050-9756ffdc2472 // indirect
	golang.org/x/lint v0.0.0-20190301231843-5614ed5bae6f
	golang.org/x/net v0.0.0-20190827160401-ba9fcec4b297 // indirect
	gopkg.in/ini.v1 v1.46.0 // indirect
	src.techknowlogick.com/xgo v0.0.0-20190904182048-b4fba6551e4a
	xorm.io/core v0.7.0
)
