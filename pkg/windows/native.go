package windows

import (
	"git.kolaente.de/konrad/Konfi-Castle-Kasino/pkg/config"
	"github.com/labstack/gommon/log"
	"github.com/pkg/browser"
)

// OpenNativeWindows opens browser windows
func OpenNativeWindows() {
	err := browser.OpenURL("http://127.0.0.1" + config.GetInterface())
	if err != nil {
		log.Error(err)
	}
	err = browser.OpenURL("http://127.0.0.1" + config.GetInterface() + "/admin")
	if err != nil {
		log.Error(err)
	}
}
