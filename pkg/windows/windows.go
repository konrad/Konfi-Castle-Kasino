package windows

import (
	"git.kolaente.de/konrad/Konfi-Castle-Kasino/pkg/config"
	"github.com/asticode/go-astilectron"
	"github.com/labstack/gommon/log"
)

// OpenWindows opens electron windows
func OpenWindows() {
	// Webview viewer
	/*	err := webview.Open("Konfi@Castle Viewer", "http://127.0.0.1:8080", 800, 600, true)
		if err != nil {
			log.Fatal(err)
		}*/

	// Astilectron Bootstrapper and bundler
	/*if err := bootstrap.Run(bootstrap.Options{
		Asset:    Asset,
		AssetDir: AssetDir,
		AstilectronOptions: astilectron.Options{
			AppName:            "Konfi@Castle Kasino",
			AppIconDarwinPath:  "gopher.icns",
			AppIconDefaultPath: "gopher.png",
		},
		RestoreAssets: RestoreAssets,
		Windows: []*bootstrap.Window{{
			Homepage:       "http://127.0.0.1:8080",
			Options: &astilectron.WindowOptions{
				BackgroundColor: astilectron.PtrStr("#333"),
				Center:          astilectron.PtrBool(true),
				Height:          astilectron.PtrInt(700),
				Width:           astilectron.PtrInt(700),
			},
		}},
	}); err != nil {
		log.Fatal("running bootstrap failed ", err)
		//astilog.Fatal(errors.Wrap(err, "running bootstrap failed"))
	}*/

	// Astilectron classic
	a, err := astilectron.New(astilectron.Options{
		AppName:            "Konfi@Castle Kasino",
		AppIconDefaultPath: "gopher.png",
		AppIconDarwinPath:  "gopher.icns",
		BaseDirectoryPath:  "astilectron-deps",
	})
	if err != nil {
		log.Error("creating new astilectron failed", err)
	}
	defer a.Close()
	a.HandleSignals()

	// Start
	if err = a.Start(); err != nil {
		log.Error("starting failed", err)
	}

	// Create Frontend window
	var wFrontend *astilectron.Window
	wFrontend, err = a.NewWindow("http://"+config.GetInterface(), &astilectron.WindowOptions{
		Center: astilectron.PtrBool(true),
		Height: astilectron.PtrInt(800),
		Width:  astilectron.PtrInt(1200),
	})
	if err != nil {
		log.Error("new window failed", err)
	}
	if err = wFrontend.Create(); err != nil {
		log.Error("creating window failed ", err)
	}

	// If several displays, move the window to the second display
	var displays = a.Displays()
	if len(displays) > 1 {
		if displays[1].IsPrimary() {
			wFrontend.MoveInDisplay(displays[0], 50, 50)
		} else {
			wFrontend.MoveInDisplay(displays[1], 50, 50)
		}
		wFrontend.Maximize()
	}

	// Create Admin window
	var wAdmin *astilectron.Window
	if wAdmin, err = a.NewWindow("http://"+config.GetInterface()+"/admin", &astilectron.WindowOptions{
		Center: astilectron.PtrBool(true),
		Height: astilectron.PtrInt(700),
		Width:  astilectron.PtrInt(1200),
	}); err != nil {
		log.Error("new window failed", err)
	}
	if err = wAdmin.Create(); err != nil {
		log.Error("creating window failed ", err)
	}

	// Blocking pattern
	a.Wait()
}
