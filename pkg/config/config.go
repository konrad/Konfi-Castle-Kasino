package config

import (
	"github.com/go-ini/ini"
	"log"
)

//Configuration Struct
type Configuration struct {
	AdminPassword string
	Interface     string
	DBFile        string
	Mode          int
	OpenWindows   bool
	OpenBrowser   bool
	SaveMetrics   bool
}

var siteConf = &Configuration{}

// InitConfig parses the config and maps out values
func InitConfig() {
	err := ini.MapTo(siteConf, "./config.ini")
	if err != nil {
		log.Fatal(err)
	}
}

// GetConfig returns the full config
func GetConfig() *Configuration {
	return siteConf
}

// GetMode returns the mode
func GetMode() int {
	return siteConf.Mode
}

// GetInterface returns the interface the server listens on
func GetInterface() string {
	return siteConf.Interface
}

// GetAdminPassword returns the admin password
func GetAdminPassword() string {
	return siteConf.AdminPassword
}

// GetDBFile returns the path to the db file
func GetDBFile() string {
	return siteConf.DBFile
}

// GetOpenWindows returns whether to open electron windows or not
func GetOpenWindows() bool {
	return siteConf.OpenWindows
}

// GetOpenBrowser returns whether to open browser windows or not
func GetOpenBrowser() bool {
	return siteConf.OpenBrowser
}

// GetSaveMetrics returns whether to use metrics or not
func GetSaveMetrics() bool {
	return siteConf.SaveMetrics
}
