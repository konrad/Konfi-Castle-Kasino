package models

import "github.com/labstack/gommon/log"

// Metric is the structure for metrics
type Metric struct {
	ID          int64 `xorm:"pk autoincr" json:"id" form:"id"`
	Kcoins      int64 `xorm:"bigint(11)"`
	CommunityID int64 `xorm:"bigint(11)"`
	CreatedUnix int64 `xorm:"created"`
}

// AddMetric saves a new metric point
func AddMetric() {
	allCommunites := []Community{}
	err := x.Find(&allCommunites)
	if err != nil {
		log.Error("Error getting metric data", err)
	}

	for _, community := range allCommunites {
		m := Metric{
			Kcoins:      community.KCoins,
			CommunityID: community.ID,
		}
		_, err := x.Insert(m)
		if err != nil {
			log.Error("Error saving metrics", err)
		}
	}
}
