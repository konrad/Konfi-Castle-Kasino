package models

// Managable represents an interface which can be managed
type Managable interface {
	ReadAll(string) (interface{}, error)
	Create() error
	Update(int64) error
	Delete() error
}
