package models

import (
	"git.kolaente.de/konrad/Konfi-Castle-Kasino/pkg/config"
	"github.com/go-xorm/xorm"
	"github.com/labstack/gommon/log"
	_ "github.com/mattn/go-sqlite3" // Needed for sqlite compatibility
	"xorm.io/core"
)

var x *xorm.Engine

// DBinit creates the initial db connection and does migrations
func DBinit() {
	var err error
	x, err = xorm.NewEngine("sqlite3", config.GetDBFile())
	if err != nil {
		log.Fatal(err)
	}

	x.SetMapper(core.GonicMapper{})
	x.ShowSQL(false)
	x.Logger().SetLevel(core.LOG_DEBUG)

	x.Sync(&Kofi{}, &Community{}, &Metric{})

	return
}
