package models

import "fmt"

// ErrKofiDoesNotExist represents an error where a kofi does not exist
type ErrKofiDoesNotExist struct {
	ID int64
}

// Error is Go's error implementation
func (err ErrKofiDoesNotExist) Error() string {
	return fmt.Sprintf("This kofi does not exist [ID: %n]", err.ID)
}
