package router

import (
	"git.kolaente.de/konrad/Konfi-Castle-Kasino/pkg/config"
	"github.com/labstack/echo/v4"
	"net/http"
)

func showList(c echo.Context) error {

	//Config
	SiteConf := config.GetConfig()

	//Template
	return c.Render(http.StatusOK, "index", SiteConf)
}
