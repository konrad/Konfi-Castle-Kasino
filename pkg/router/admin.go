package router

import (
	"git.kolaente.de/konrad/Konfi-Castle-Kasino/pkg/models"
	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
	"net/http"
	"strconv"

	"git.kolaente.de/konrad/Konfi-Castle-Kasino/pkg/config"
)

// AdminInfos represents stuff about an admin
type AdminInfos struct {
	Loggedin bool
	Mode     int
	Version  string
}

func isLoggedIn(c echo.Context) bool {
	sess, _ := session.Get(sessionName, c)
	_, is := sess.Values[sessionKey]
	return is
}

func adminHandler(c echo.Context) error {
	adminInfos := AdminInfos{
		Loggedin: true,
		Mode:     config.GetMode(),
		Version:  models.Version,
	}
	if isLoggedIn(c) {
		return c.Render(http.StatusOK, "admin_mode_"+strconv.Itoa(config.GetMode()), adminInfos)
	}

	adminInfos.Loggedin = false
	return c.Render(http.StatusOK, "login", adminInfos)
}
