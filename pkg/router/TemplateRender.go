package router

import (
	"github.com/labstack/echo/v4"
	"html/template"
	"io"
)

// Template represents a template
type Template struct {
	templates *template.Template
}

// Render implements echo's template handler
func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}
