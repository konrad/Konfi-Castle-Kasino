package router

import (
	"net/http"

	"github.com/gorilla/sessions"
	"github.com/labstack/echo-contrib/session"
	"github.com/labstack/echo/v4"
)

func logout(c echo.Context) error {
	sess, _ := session.Get(sessionName, c)
	sess.Options = &sessions.Options{
		MaxAge: 86400 * -7,
	}
	sess.Values[sessionKey] = false
	err := sess.Save(c.Request(), c.Response())
	if err != nil {
		return echo.ErrInternalServerError
	}

	return c.Redirect(http.StatusFound, "/admin")
}
