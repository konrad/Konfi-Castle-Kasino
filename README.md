# Konfi-Castle-Kasino

[![Build Status](https://drone.kolaente.de/api/badges/konrad/Konfi-Castle-Kasino/status.svg)](https://drone.kolaente.de/konrad/Konfi-Castle-Kasino)
[![Download](https://img.shields.io/badge/download-v1.3-brightgreen.svg)](https://storage.kolaente.de/minio/konfi-castle-casino/)

TODO::

* ~~Coins updaten~~
* Kofis Hinzufügen
  * ~~Manuell (einzeln)~~
  * CSV-Import
* ~~Kofis löschen~~
* ~~Logout~~
* Konfis auf der Frontseite mit Websockets updaten
* ~~Mode hinzufügen: in der Config soll man zwischen Gemeinden und Konfis umschalten können, so dass entweder die Gemeinden oder Konfis gegeneinander spielen~~
* ~~Alles in Fenster packen~~
* ~~Alles an schriften/Bildern/Etc Herunterladen, damit alles auch offline funktioniert~~
* ~~Random Port (wenn man irgendwie den laufenden port finden kann)~~
* ~~Front-Tabelle schöner~~
* ~~Version einbauen, soll in der Konsole und im Adminbereich angezeigt werden (dezent)~~
* Mini-Doku
  * Inbetriebnahme
  * Bedienung

Release: 3 Zips. Enthalten jeweils die kompilierten binaries, Templates, JS/CSS/Fonts/etc. Sozusagen Ready-to-Run.
* Win
* macOS
* Linux
