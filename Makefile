DIST := dist
ELECTRON_VERSION := v4.0.1
IMPORT := git.mowie.cc/konrad/Konfi-Castle-Kasino

SED_INPLACE := sed -i

ifeq ($(OS), Windows_NT)
	EXECUTABLE := Konfi-Castle-Kasino.exe
else
	EXECUTABLE := Konfi-Castle-Kasino
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Darwin)
		SED_INPLACE := sed -i ''
	endif
endif

RELEASE_NAME := Konfi-Castle-Kasino

GOFILES := $(shell find . -name "*.go" -type f ! -path "./vendor/*" ! -path "*/bindata.go")
GOFMT ?= gofmt -s

GOFLAGS := -v -mod=vendor
EXTRA_GOFLAGS ?=

LDFLAGS := -X "git.kolaente.de/konrad/Konfi-Castle-Kasino/pkg/models.Version=$(shell git describe --tags --always --abbrev=10 | sed 's/-/+/' | sed 's/^v//' | sed 's/-g/-/')" -X "main.Tags=$(TAGS)"

PACKAGES ?= $(shell go list ./... | grep -v /vendor/)
SOURCES ?= $(shell find . -name "*.go" -type f)

TAGS ?=

TMPDIR := $(shell mktemp -d 2>/dev/null || mktemp -d -t 'kasino-temp')

ifeq ($(OS), Windows_NT)
	EXECUTABLE := Konfi-Castle-Kasino.exe
else
	EXECUTABLE := Konfi-Castle-Kasino
endif

ifneq ($(DRONE_TAG),)
	VERSION ?= $(subst v,,$(DRONE_TAG))
else
	ifneq ($(DRONE_BRANCH),)
		VERSION ?= $(subst release/v,,$(DRONE_BRANCH))
	else
		VERSION ?= master
	endif
endif

.PHONY: all
all: build

.PHONY: clean
clean:
	go clean -i ./...
	rm -rf $(EXECUTABLE) $(DIST) $(BINDATA)

required-gofmt-version:
	@go version  | grep -q '\(1.7\|1.8\)' || { echo "We require go version 1.7 or 1.8 to format code" >&2 && exit 1; }

.PHONY: fmt
fmt: required-gofmt-version
	$(GOFMT) -w $(GOFILES)

.PHONY: fmt-check
fmt-check: required-gofmt-version
	# get all go files and run go fmt on them
	@diff=$$($(GOFMT) -d $(GOFILES)); \
	if [ -n "$$diff" ]; then \
		echo "Please run 'make fmt' and commit the result:"; \
		echo "$${diff}"; \
		exit 1; \
	fi;

.PHONY: install
install: $(wildcard *.go)
	go install -v -tags '$(TAGS)' -ldflags '-s -w $(LDFLAGS)'

.PHONY: build
build: $(EXECUTABLE)

$(EXECUTABLE): $(SOURCES)
	go build $(GOFLAGS) $(EXTRA_GOFLAGS) -tags '$(TAGS)' -ldflags '-s -w $(LDFLAGS)' -o $@

.PHONY: release
release: release-dirs release-windows release-linux release-darwin release-copy release-check release-os-package release-download-deps release-electron-prepare release-electron-bundle release-zip

.PHONY: release-dirs
release-dirs:
	mkdir -p $(DIST)/binaries $(DIST)/release $(DIST)/zip

.PHONY: release-windows
release-windows:
	@hash xgo > /dev/null 2>&1; if [ $$? -ne 0 ]; then \
		go install $(GOFLAGS) src.techknowlogick.com/xgo; \
	fi
	xgo -dest $(DIST)/binaries -tags 'netgo $(TAGS)' -ldflags '-linkmode external -extldflags "-static" $(LDFLAGS)' -targets 'windows/*' -out $(RELEASE_NAME)-$(VERSION) .
ifneq ($(DRONE_WORKSPACE),'')
	mv /build/* $(DIST)/binaries
endif

.PHONY: release-linux
release-linux:
	@hash xgo > /dev/null 2>&1; if [ $$? -ne 0 ]; then \
		go install $(GOFLAGS) src.techknowlogick.com/xgo; \
	fi
	xgo -dest $(DIST)/binaries -tags 'netgo $(TAGS)' -ldflags '-linkmode external -extldflags "-static" $(LDFLAGS)' -targets 'linux/*' -out $(RELEASE_NAME)-$(VERSION) .
ifneq ($(DRONE_WORKSPACE),'')
	mv /build/* $(DIST)/binaries
endif

.PHONY: release-darwin
release-darwin:
	@hash xgo > /dev/null 2>&1; if [ $$? -ne 0 ]; then \
		go install $(GOFLAGS) src.techknowlogick.com/xgo; \
	fi
	xgo -dest $(DIST)/binaries -tags 'netgo $(TAGS)' -ldflags '$(LDFLAGS)' -targets 'darwin/*' -out $(RELEASE_NAME)-$(VERSION) .
ifneq ($(DRONE_WORKSPACE),'')
	mv /build/* $(DIST)/binaries
endif

.PHONY: release-copy
release-copy:
	$(foreach file,$(wildcard $(DIST)/binaries/$(EXECUTABLE)-*),cp $(file) $(DIST)/release/$(notdir $(file));)

.PHONY: release-check
release-check:
	cd $(DIST)/release; $(foreach file,$(wildcard $(DIST)/release/$(EXECUTABLE)-*),sha256sum $(notdir $(file)) > $(notdir $(file)).sha256;)

.PHONY: release-download-deps
release-download-deps:
	mkdir -p $(DIST)/astilectron-deps; \
	wget https://github.com/asticode/astilectron/archive/v0.30.0.zip -O $(DIST)/astilectron-deps/astilectron-v0.30.0.zip; \
	wget https://github.com/electron/electron/releases/download/$(ELECTRON_VERSION)/electron-$(ELECTRON_VERSION)-darwin-x64.zip -O $(DIST)/astilectron-deps/electron-darwin-amd64-$(ELECTRON_VERSION).zip; \
	wget https://github.com/electron/electron/releases/download/$(ELECTRON_VERSION)/electron-$(ELECTRON_VERSION)-linux-ia32.zip -O $(DIST)/astilectron-deps/electron-linux-ia32-$(ELECTRON_VERSION).zip; \
	wget https://github.com/electron/electron/releases/download/$(ELECTRON_VERSION)/electron-$(ELECTRON_VERSION)-linux-x64.zip -O $(DIST)/astilectron-deps/electron-linux-amd64-$(ELECTRON_VERSION).zip; \
	wget https://github.com/electron/electron/releases/download/$(ELECTRON_VERSION)/electron-$(ELECTRON_VERSION)-linux-armv7l.zip -O $(DIST)/astilectron-deps/electron-linux-arm-$(ELECTRON_VERSION).zip; \
	wget https://github.com/electron/electron/releases/download/$(ELECTRON_VERSION)/electron-$(ELECTRON_VERSION)-win32-ia32.zip -O $(DIST)/astilectron-deps/electron-win32-ia32-$(ELECTRON_VERSION).zip; \
	wget https://github.com/electron/electron/releases/download/$(ELECTRON_VERSION)/electron-$(ELECTRON_VERSION)-win32-x64.zip -O $(DIST)/astilectron-deps/electron-win32-amd64-$(ELECTRON_VERSION).zip;

.PHONY: release-electron-prepare
release-electron-prepare:
	mkdir -p $(DIST)/astilectron-deps/electron-darwin-amd64; \
	mkdir -p $(DIST)/astilectron-deps/electron-linux-ia32; \
	mkdir -p $(DIST)/astilectron-deps/electron-linux-amd64; \
	mkdir -p $(DIST)/astilectron-deps/electron-linux-arm; \
	mkdir -p $(DIST)/astilectron-deps/electron-win32-ia32; \
	mkdir -p $(DIST)/astilectron-deps/electron-win32-amd64; \
	unzip $(DIST)/astilectron-deps/astilectron-v0.30.0.zip -d $(DIST)/astilectron-deps/; \
	mv $(DIST)/astilectron-deps/astilectron-0.30.0 $(DIST)/astilectron-deps/astilectron; \
	unzip $(DIST)/astilectron-deps/electron-darwin-amd64-$(ELECTRON_VERSION).zip -d $(DIST)/astilectron-deps/electron-darwin-amd64; \
	unzip $(DIST)/astilectron-deps/electron-linux-ia32-$(ELECTRON_VERSION).zip -d $(DIST)/astilectron-deps/electron-linux-ia32; \
	unzip $(DIST)/astilectron-deps/electron-linux-amd64-$(ELECTRON_VERSION).zip -d $(DIST)/astilectron-deps/electron-linux-amd64; \
	unzip $(DIST)/astilectron-deps/electron-linux-arm-$(ELECTRON_VERSION).zip -d $(DIST)/astilectron-deps/electron-linux-arm; \
	unzip $(DIST)/astilectron-deps/electron-win32-ia32-$(ELECTRON_VERSION).zip -d $(DIST)/astilectron-deps/electron-win32-ia32; \
	unzip $(DIST)/astilectron-deps/electron-win32-amd64-$(ELECTRON_VERSION).zip -d $(DIST)/astilectron-deps/electron-win32-amd64;

.PHONY: release-os-package
release-os-package:
	$(foreach file,$(filter-out %.sha256,$(wildcard $(DIST)/release/$(EXECUTABLE)-*)),mkdir $(file)-full/astilectron-deps/vendor -p;mkdir $(file)-full/astilectron-deps/vendor/astilectron -p;mv $(file) $(file)-full/;mv $(file).sha256 $(file)-full/; cp config.ini.sample $(file)-full/config.ini; cp LICENSE $(file)-full/; cp assets/ $(file)-full/ -R; cp tpl $(file)-full/ -R; cp gopher.png $(file)-full/; cp gopher.icns $(file)-full/; )

.PHONY: release-electron-bundle
release-electron-bundle:
	cp -r $(DIST)/astilectron-deps/electron-darwin-amd64 $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-darwin-10.6-amd64-full/astilectron-deps/vendor/electron-darwin-amd64; \
	cp -r $(DIST)/astilectron-deps/electron-darwin-amd64 $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-darwin-10.6-amd64-full/astilectron-deps/vendor/electron-darwin-amd64; \
	cp -r $(DIST)/astilectron-deps/electron-linux-ia32   $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-linux-386-full/astilectron-deps/vendor/electron-linux-ia32; \
	cp -r $(DIST)/astilectron-deps/electron-linux-amd64  $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-linux-amd64-full/astilectron-deps/vendor/electron-linux-amd64; \
	cp -r $(DIST)/astilectron-deps/electron-linux-arm    $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-linux-arm-5-full/astilectron-deps/vendor/electron-linux-arm; \
	cp -r $(DIST)/astilectron-deps/electron-linux-arm    $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-linux-arm-6-full/astilectron-deps/vendor/electron-linux-arm; \
	cp -r $(DIST)/astilectron-deps/electron-linux-arm    $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-linux-arm-7-full/astilectron-deps/vendor/electron-linux-arm; \
	cp -r $(DIST)/astilectron-deps/electron-linux-arm    $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-linux-arm64-full/astilectron-deps/vendor/electron-linux-arm; \
	cp -r $(DIST)/astilectron-deps/electron-win32-ia32   $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-windows-4.0-386.exe-full/astilectron-deps/vendor/electron-win32-ia32; \
	cp -r $(DIST)/astilectron-deps/electron-win32-amd64  $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-windows-4.0-amd64.exe-full/astilectron-deps/vendor/electron-win32-amd64; \
	cp -r $(DIST)/astilectron-deps/astilectron $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-darwin-10.6-386-full/astilectron-deps/vendor/; \
	cp -r $(DIST)/astilectron-deps/astilectron $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-darwin-10.6-amd64-full/astilectron-deps/vendor/; \
	cp -r $(DIST)/astilectron-deps/astilectron $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-linux-386-full/astilectron-deps/vendor/; \
	cp -r $(DIST)/astilectron-deps/astilectron $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-linux-amd64-full/astilectron-deps/vendor/; \
	cp -r $(DIST)/astilectron-deps/astilectron $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-linux-arm-5-full/astilectron-deps/vendor/; \
	cp -r $(DIST)/astilectron-deps/astilectron $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-linux-arm-6-full/astilectron-deps/vendor/; \
	cp -r $(DIST)/astilectron-deps/astilectron $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-linux-arm-7-full/astilectron-deps/vendor/; \
	cp -r $(DIST)/astilectron-deps/astilectron $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-linux-arm64-full/astilectron-deps/vendor/; \
	cp -r $(DIST)/astilectron-deps/astilectron $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-windows-4.0-386.exe-full/astilectron-deps/vendor/; \
	cp -r $(DIST)/astilectron-deps/astilectron $(DIST)/release/$(RELEASE_NAME)-$(VERSION)-windows-4.0-amd64.exe-full/astilectron-deps/vendor/;

.PHONY: release-zip
release-zip:
	$(foreach file,$(wildcard $(DIST)/release/$(EXECUTABLE)-*),cd $(file); zip -r ../../zip/$(shell basename $(file)).zip *; cd ../../../; )

.PHONY: lint
lint:
	@hash golint > /dev/null 2>&1; if [ $$? -ne 0 ]; then \
		go install $(GOFLAGS) golang.org/x/lint/golint; \
	fi
	for PKG in $(PACKAGES); do golint -set_exit_status $$PKG || exit 1; done;
